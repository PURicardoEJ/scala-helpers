package com.ricardoescalon

import org.specs2.mutable._
import com.ricardoescalon._
import com.ricardoescalon.debug.DebugUtils

case class DebugUtilsSpec extends Specification {
  "DebugUtils" should {
    "print to the console if in the debug mode" in {
    	val debug = true
    	val stream = new java.io.ByteArrayOutputStream()
    	val s1 = "this"
    	Console.withOut(stream) {
    		DebugUtils.println(debug, s1)
    	}
    	stream.toString() must beEqualTo(s1).ignoreSpace 
    }
  }

}