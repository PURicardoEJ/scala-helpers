package com.ricardoescalon.debug
/**
 * Helpers for common debugging functions.
 */
object DebugUtils {
  /**
   * Prints to console if true
   * @param debug set to true to print, set to false to ignore
   * @string pass as string to print
   */
	def println(debug: Boolean, string: String) {
		if (debug)
		  Console.println(string)
	}
}